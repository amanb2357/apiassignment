package com.sapient.APIassignment.APIAssignment;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


public class pid_finder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DefaultHttpClient httpClient = new DefaultHttpClient();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String playerNameString = request.getParameter("fullname");
		String playerNameStringUrlString = playerNameString.replace(" ", "%20");
		String urlString = "https://cricapi.com/api/playerFinder?apikey=sAJUpzchqWY6B1uE6p1X5K6NYCN2&name=" + playerNameStringUrlString;
		HttpGet getRequest = new HttpGet(urlString);
		getRequest.addHeader("accept", "application/json");
		HttpResponse responseReceived = httpClient.execute(getRequest);
		int statusCode = responseReceived.getStatusLine().getStatusCode();
        if (statusCode != 200)
        {
            throw new RuntimeException("Failed with HTTP error code : " + statusCode);
        }
        HttpEntity httpEntity = responseReceived.getEntity();
        String apiOutput = EntityUtils.toString(httpEntity);
        Gson gson = new GsonBuilder().create();
        JsonArray jsonArray = gson.fromJson(apiOutput, JsonObject.class).getAsJsonArray("data");
        JsonObject dataObject = (JsonObject) jsonArray.get(0);
        String pidString = dataObject.get("pid").toString();
        request.setAttribute("pidString", pidString);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("player_finder");
        requestDispatcher.forward(request, response);
	}

}
