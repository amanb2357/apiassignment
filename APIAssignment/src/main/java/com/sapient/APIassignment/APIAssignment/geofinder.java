package com.sapient.APIassignment.APIAssignment;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Servlet implementation class geofinder
 */
public class geofinder extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	DefaultHttpClient httpClient = new DefaultHttpClient();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String addressString = (String) request.getParameter("address");
		String addressURLString = addressString.replace(" ", "+");
		String keyString = "AIzaSyBpv7_tKVO5svTnTDUdfWz-fM3qpash6Gw";
		String URLString = "https://maps.googleapis.com/maps/api/geocode/json?address=" + addressURLString + "&key=" + keyString;
		HttpGet getRequest = new HttpGet(URLString);
		getRequest.addHeader("accept", "application/json");
		HttpResponse responseReceived = httpClient.execute(getRequest);
		int statusCode = responseReceived.getStatusLine().getStatusCode();
        if (statusCode != 200)
        {
            throw new RuntimeException("Failed with HTTP error code : " + statusCode);
        }
        HttpEntity httpEntity = responseReceived.getEntity();
        String apiOutput = EntityUtils.toString(httpEntity);
        request.setAttribute("location", apiOutput);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("geolocation.jsp");
        requestDispatcher.forward(request, response);
	}
}
