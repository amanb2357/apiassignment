package com.sapient.APIassignment.APIAssignment;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;


public class player_finder extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	DefaultHttpClient httpClient = new DefaultHttpClient();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String pidString = (String) request.getAttribute("pidString");
		String urlString = "https://cricapi.com/api/playerStats?apikey=sAJUpzchqWY6B1uE6p1X5K6NYCN2&pid=" + pidString;
		HttpGet getRequest = new HttpGet(urlString);
		getRequest.addHeader("accept", "application/json");
		HttpResponse responseReceived = httpClient.execute(getRequest);
		int statusCode = responseReceived.getStatusLine().getStatusCode();
        if (statusCode != 200)
        {
            throw new RuntimeException("Failed with HTTP error code : " + statusCode);
        }
        HttpEntity httpEntity = responseReceived.getEntity();
        String apiOutput = EntityUtils.toString(httpEntity);
        request.setAttribute("Player_Info", apiOutput);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("player.jsp");
        requestDispatcher.forward(request, response);
	}

}
